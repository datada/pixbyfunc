var SS = {
  ROW_SIZE: 10,
  COL_SIZE: 10,
  utils: {},
  actions: {}
};

SS.utils.make_array = function (size, proc) {
	var arr = [];
	var i;
	for (i=0; i<size; i+=1) {
		arr.push( proc(i) );	
	}
	return arr;	
};

SS.utils.make_matrix = function (row, col, proc) {
  
  var m = SS.utils.make_array(row, function (r) {
    return SS.utils.make_array(col, function (c) {
      return proc(r, c);
    });
  });
  
  return m;
};

SS.initialize_cells = function (proc) {
  //give test code to customize init
  proc = proc ? proc : function (r, c) {
    return {val:null, expr:null, parents:[], children:[]};
  };
  return SS.utils.make_matrix(SS.ROW_SIZE, SS.COL_SIZE, proc);
};

SS.cell_at = function (id) {
  //log("looking at cell "+JSON.stringify(id));
  return SS.cells[id.row-1][id.col-1];
};

// return {col:c, row:r}'s for cell(r, c)
SS.eval_cell_defs = function (expr, cell_id) {
   var context = {};
   context['cell'] = function (_c, _r) {
     var id = {row:(cell_id.row + _r), col:(cell_id.col + _c)};
     log("about to return cell id "+JSON.stringify(id));
     return id;
   };
   //upper left corner to lower right corner
   context['cells'] = function (_c1,_r1,_c2,_r2) {
      var r_min = cell_id.row + _r1;
      var r_max = cell_id.row + _r2;
      var c_min = cell_id.col + _c1;
      var c_max = cell_id.col + _c2;
      
      var cells = [], r, c;
      for (r=r_min;r<=r_max;r+=1) {
         for (c=c_min;c<=c_max;c+=1) {
            cells.push( {row:r, col:c} );
         }
      }
      
      return cells;
   };
   context['$cell'] = function (c,r) {
      return {row:r, col:c};
   };
   context['$cells'] = function (c1,r1,c2,r2) {
      var cells = [], r, c;
      for (r=r1;r<=r2;r+=1) {
         for (c=c1;c<=c2;c+=1) {
            cells.push( {row:r, col:c} );
         }
      }
      
      return cells;
   };
   with ( context ) {

      var val = eval("("+ expr +")");
      log("eval_cell_defs is about to return val "+JSON.stringify(val));
      
      return val;
   }
};
// {col:c, row:c}'s are further ref'd to cells for cell(r, c)
SS.eval_expr_at = function (expr, cell_id) {
   var context = {};
   context['cell'] = function (_c,_r) {
     var val = SS.cell_at({row:(cell_id.row + _r), col:(cell_id.col + _c)})['val'];
     log("about to return cell val "+JSON.stringify(val));
     return val;
   };
   //upper left corner to lower right corner
   context['cells'] = function (_c1,_r1,_c2,_r2) {
      var r_min = cell_id.row + _r1;
      var r_max = cell_id.row + _r2;
      var c_min = cell_id.col + _c1;
      var c_max = cell_id.col + _c2;
      
      var cells = [], r, c;
      for (r=r_min;r<=r_max;r+=1) {
         for (c=c_min;c<=c_max;c+=1) {
            cells.push( SS.cell_at({row:r, col:c})['val'] );
         }
      }
      
      return cells;
   };
   context['$cell'] = function (c,r) {
      return SS.cell_at({row:r, col:c})['val'];
   };
   context['$cells'] = function (c1,r1,c2,r2) {
      var cells = [], r, c;
      for (r=r1;r<=r2;r+=1) {
         for (c=c1;c<=c2;c+=1) {
            cells.push( SS.cell_at({row:r, col:c})['val'] );
         }
      }
      
      return cells;
   };
   with ( context ) {
     
      var val = eval("("+ expr +")");
      log("eval_expr_at is about to return val "+JSON.stringify(val));
      
      return val;
   }
};

// used in dependency mgmt
// $cell(4,4) => {row:4, col:4}
SS.extract_ids_at_buggy = function (expr, cell_id) {
  var txt = new String(expr);
  var invocations = [];
  // (?: ...) non capturing 
  // ^|\s beg of string or white space
  // \( \) left paren right paren
  // [^)] any except right paren
  //var re = /(?:^|\s)([\$]{0,1}cell[s]{0,1}\([^)]*\))(?=\s|$)/ig;
  var re = /([\$]{0,1}cell[s]{0,1}\([^)]*\))(?=\s|$)/ig;

  var found  = null;
  while ( (found = re.exec(txt)) !== null ) {
     log( "looking at "+txt.substr(re.lastIndex) );
     //txt = txt.substr( re.lastIndex );//remaining txt to process
     invocations.push( found[1] );
  }
  //return invocations;
  return $.map(invocations, function (proc_expr) {
     return SS.eval_cell_defs(proc_expr, cell_id);
  });
  
};

SS.parse_cell_fns = function ( src ) {
  var txt = src.toLowerCase();
  var rest;
  var index = -1;
  var curr_char = '';
  var curr_bucket = '';
  var fns = [];
  
  var next = function () {
     if (txt.length <= index) return false;
     
     index += 1;
     curr_char = txt.charAt( index );
     return curr_char;
  };
  
  // assuming no nesting of parens...
  var work = function ( pattern ) {
     log("pattern "+pattern);
     curr_bucket += pattern;
     index += pattern.length - 1;
     
     while (')' !== next()) {
        curr_bucket += curr_char;
     }
     if (')' === curr_char) {
        curr_bucket += curr_char;
     }
     fns.push( curr_bucket );
     curr_bucket = '';
     
  };
  
  while( next() ) {
     
     rest = txt.slice( index );
     
     if (0 === rest.indexOf("$cells(")) {
        work("$cells(");
     } else if (0 === rest.indexOf("$cell(")) {
        work("$cell(");
     } else if (0 === rest.indexOf("cells(")) {
        work("cells(");
     } else if (0 === rest.indexOf("cell(")) {
        work("cell(");
     } else {
        curr_bucket = '';
     }
  }
  return fns;
};

SS.extract_ids_at = function (expr, cell_id) {

  var invocations = SS.parse_cell_fns( expr );

  //return invocations;
  return $.map(invocations, function (proc_expr) {
     return SS.eval_cell_defs(proc_expr, cell_id);
  });
  
};

// A or a to 1
SS.letter_to_number = function ( azAZ ) {
   return azAZ.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0) + 1;
};
// 1 to A; 26 to Z
SS.number_to_AZ = function (num) {
  return 'abcdefghijklmnopqrstuvwxyz'.charAt(num-1).toUpperCase();
};

// d5 => {col:4, row:5}
SS.cell_id_for_cell_name = function ( aZ09 ) {
   var letters_numerics = SS.separate_letters_numerics( aZ09, [], []);

   return {
      col:SS.letter_to_number( letters_numerics[0] ),
      row:parseInt( letters_numerics[1], 10)
   };
};

SS.cell_name_for_cell_id = function (id) {
  return SS.number_to_AZ(id.col) + id.row ;
};

SS.is_cell_name = function (txt) {
   var re = /^([a-zA-Z]+)(\d+)$/ig;//looking for A2, z30, and such
   return re.exec( txt );
};

// D23 => ["D", "23"]
SS.separate_letters_numerics = function (str, letters, numerics) {
  if (0 < str.length) {
     var ch = str.charAt(0);
     if ('0' <= ch && ch <= '9')
         numerics.push( ch );
     else
         letters.push( ch );
     return SS.separate_letters_numerics(str.substr(1), letters, numerics);
  }
  return [letters.join(""), numerics.join("")];
};

SS.is_numeric = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

SS.is_letter = function (str) {
    return /^[a-z]+$/i.test(str);
};
