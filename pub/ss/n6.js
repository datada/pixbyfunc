window.SS = window.SS ? window.SS : (function (R, C) {

var SS = {
  ROW_SIZE: R,
  COL_SIZE: C,
  rows: [],
  selected_cell_id: {row:1, col:1},
  actions: {}
};

// initialize cells
(function () {
  var r, c;
  for (r=1; r<=SS.ROW_SIZE; r+=1) {
    var row = [];
    for (c=1; c<=SS.COL_SIZE; c+=1) {
      row.push({val: null, expr: null});
    }
    SS.rows.push( row );
  }
})();

// because of A-Z column names
// A or a to 1
SS.letter_to_number = function ( azAZ ) {
	return azAZ.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0) + 1;
};
if (SS.letter_to_number("A") !== 1)
	throw "A => 1 failed";

// 1 to A; 26 to Z
SS.num_to_AZ = function (num) {
	return this.num_to_az(num).toUpperCase();
},

SS.num_to_az = function (num) {
	return 'abcdefghijklmnopqrstuvwxyz'.charAt(num-1); 
};
// end of colnames related

// need to know c5 is col3 row 5
// D23 => ["D", "23"]
SS.separate_letters_numerics = function (str, letters, numerics) {
 if (0 < str.length) {
    var ch = str.charAt(0);
    if ('0' <= ch && ch <= '9')
        numerics.push( ch );
    else
        letters.push( ch );
    return this.separate_letters_numerics(str.substr(1), letters, numerics);
 }
 return [letters.join(""), numerics.join("")];
};

SS.is_cell_name = function (txt) {
  var re = /^([a-zA-Z]+)(\d+)$/ig;//looking for A2, z30, and such
  return re.exec( txt );
};

// d5 => {col:4, row:5}
SS.cell_id_for_cell_name = function ( aZ09 ) {
  var letters_numerics = this.separate_letters_numerics( aZ09, [], []);

  return {
     col: SS.letter_to_number( letters_numerics[0] ),
     row: parseInt( letters_numerics[1], 10)
  };
};

// SS.cell_name_for_cell_id = function (id) {
//  return this.num_to_AZ(id.col) + id.row ;
// };

SS.is_numeric = function (n) {
 return !isNaN(parseFloat(n)) && isFinite(n);
};

SS.is_letter = function (str) {
   return /^[a-z]+$/i.test(str);
};

SS.is_valid_cell_id = function (new_id) {
   
   if (new_id.col < 1 || new_id.row < 1)
      return false
   if (this.COL_SIZE < new_id.col || this.ROW_SIZE < new_id.row)
      return false;
      
   return true;
};

SS.change_selection_if_valid = function (new_id) {
   if (!this.is_valid_cell_id(new_id))
      throw "Cell out of bounds!";
      
   this.selected_cell_id = new_id;
   return true;
};

// 1 based to 0 based counting
SS.cell_for_ID = function (cell_id) {
  return this.rows[ cell_id.row - 1 ][ cell_id.col -1 ];
};

SS.cell_value = function (col, row) {
 return this.cell_for_ID({row:row, col:col})['val'];
};

SS.set_cell_expr = function (expr, id) {
  //log("actually setting "+expr+" into ("+id.row+","+id.col+")");

  var row = id.row;
  var col = id.col;
  this.cell_for_ID(id)['expr'] = expr;
};

SS.set_cell_value = function (val, id) {
  //log("finally set value "+val+" into ("+id.row+","+id.col+")");
  this.cell_for_ID(id)['val'] = val;
};

// for now no not trigger cascading changes
SS.put_expr = function (expr_or, id) {

  var expr = ("out-of-bounds" === expr_or) ? null : expr_or;
  this.set_cell_expr(expr, id);
  this.set_cell_value(expr, id);
};

// pin_down related
// convert cell(c) * B1 => cell(c) * cell(b,1) then to be handed off to pin_down
SS.sub_cell_names = function ( src ) {
 var tokens = $.map(src.split(" "), function (token) {
    return $.trim(token);
 });
 var new_tokens = [];

// var re = /(?:^|\s)(\w+)(\d+)(?=\s|$)/ig;//looking for A2, z30, and such
 var re = /^([a-zA-Z]+)(\d+)$/ig;//looking for A2, z30, and such

 var result = null;

 $.each(tokens, function (i, token) {
    result = re.exec( token );
    if (result) {
       new_tokens.push( "cell("+result[1]+","+result[2]+")" ); //cell(b,1)
    } else {
       new_tokens.push( token );
    }
 });

 return new_tokens.join(" "); 
};

SS.inside_parens = function ( str ) {
 var left = str.indexOf('(');
 var right = str.indexOf(')');
 return str.substring(left+1,right); 
};

// cell(<2, >3) w.r.t. (4, 4) => cell_value(2,7)
SS.pin_down_cell = function (cell_syntax, ref_id) {


  var args = $.map(this.inside_parens( cell_syntax ).split(","), function (arg) {
     return $.trim(arg);
  });

   if (0 === args.length)
     throw "Bad cell spec: "+cell_syntax;

   if (1 === args.length) {
      if (this.is_numeric( args[0] )) { //chose row
         return "cell_value("+ref_id.col+","+args[0]+")";
      }
      if (this.is_letter( args[0] )) { //chose col
         return "cell_value("+this.letter_to_number( args[0] )+","+ref_id.row+")"
      }
      throw "Bad cell spec: "+cell_syntax;
   }

   var col = this.pin_down_col(args[0], ref_id.col);
   var row = this.pin_down_row(args[1], ref_id.row);

   if ( this.is_valid_cell_id({row:row, col:col}) ) {
      return "cell_value("+col+","+row+")";
   }
   return "out-of-bounds";
};


SS.pin_down = function (src, ref_id) {

  if (!src)
    return null;

  var convert = function (substr, p1, offset, s) {
    return " "+SS.pin_down_cell(p1, ref_id)+" ";
  };

  var txt = new String( this.sub_cell_names(src) );
  log("after converting cell names: "+txt);
  var re = /(?:^|\s)(cell\([^)]*\))(?=\s|$)/g;// match cell() taking into consideration beg, end, white spaces around it

  return txt.replace(re, convert);
};

// end of pin_down related

// put related
SS.put_formula_in_cell = function (formula, id) {
  return this.put_expr(this.pin_down(formula, id), id);
};

SS.try_putting = function (formula, id) {
  // log("try putting "+formula+" into ("+id.col+","+id.row+")");

 if (!formula)
  return this.put_formula_in_cell(null, id);//empty the cell
 if (this.cell_value(id.col, id.row)) {
    //not empty so skip
 } else {
    return this.put_formula_in_cell(formula, id);//was empty     
 }

 return "not doing";
};

SS.put_all_helper = function (formula, i, max, id_maker) {
 var id = id_maker(i);
 // log("put "+formula+" into ("+id.col+","+id.row+")");
 if (max < i)
  return "done";

 this.try_putting(formula, id);
 return this.put_all_helper(formula, (i+1), max, id_maker);
};

SS.put_formula_in_all_cells_in_col = function ( formula, col ) {
  // log("put into all cells in col: "+col);
  return this.put_all_helper(formula, 1, this.ROW_SIZE, function (row) {
     return {row:row, col:col};
  });
};

SS.put_formula_in_all_cells_in_row = function ( formula, row ) {
  // log("put into all cells in row: "+row);

  return this.put_all_helper(formula, 1, this.COL_SIZE, function (col) {
     return {row:row, col:col};
  });
};

// end of put related

SS.actions = {
  select: function (cell_name) {
    SS.change_selection_if_valid( SS.cell_id_for_cell_name( cell_name ) );
  },
  left: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        col: (SS.selected_cell_id.col-delta),
        row: SS.selected_cell_id.row
     }
     return SS.change_selection_if_valid( new_id );
  },
  right: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        col: (SS.selected_cell_id.col+delta),
        row: SS.selected_cell_id.row
     }
     return SS.change_selection_if_valid( new_id );
  },
  up: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        row: (SS.selected_cell_id.row-delta),
        col: SS.selected_cell_id.col
     }
     return SS.change_selection_if_valid( new_id );
  },
  down: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        row: (SS.selected_cell_id.row+delta),
        col: SS.selected_cell_id.col
     }
     return SS.change_selection_if_valid( new_id );
  },
  h: function (delta) {
     return this.left(delta);
  },
  j: function (delta) {
     return this.down(delta);
  },
  k: function (delta) {
     return this.up(delta);
  },
  l: function (delta) {
     return this.right(delta);
  },
  put: function (formula, where) {     
     if (!where) {
        return SS.put_formula_in_cell( formula, SS.selected_cell_id );
     }
     if (SS.is_cell_name( where )) {
        return SS.put_formula_in_cell( formula, SS.cell_id_for_cell_name( where ) );
     }
     if (SS.is_letter( where )) {
        return SS.put_formula_in_all_cells_in_col( formula, SS.letter_to_number( where ) );
     }
     if (SS.is_numeric( where )) {
        return SS.put_formula_in_all_cells_in_row( formula, parseInt(where, 10) );
     }
  },//end of put action
  tmp: function (arg) {
  },
  etc: null 
};

return SS;
})(30, 26);