window.SS = window.SS ? window.SS : (function (R, C) {

var SS = {
  ROW_SIZE: R,
  COL_SIZE: C
};

// because colum names use A-Z
// A or a to 1
SS.letter_to_number = function ( azAZ ) {
	return azAZ.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0) + 1;
};
	// 1 to A; 26 to Z
SS.num_to_AZ = function (num) {
	return this.num_to_az(num).toUpperCase();
},

SS.num_to_az = function (num) {
	return 'abcdefghijklmnopqrstuvwxyz'.charAt(num-1); 
};

return SS;
})(30, 26);