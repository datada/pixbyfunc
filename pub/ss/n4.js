window.SS = window.SS ? window.SS : (function (R, C) {

var SS = {
  ROW_SIZE: R,
  COL_SIZE: C,
  rows: [],
  selected_cell_id: {row:1, col:1},
  actions: {}
};

// need some cells to put things into
(function () {
  var r, c;
  for (r=1; r<=SS.ROW_SIZE; r+=1) {
    var row = [];
    for (c=1; c<=SS.COL_SIZE; c+=1) {
      row.push({val: null, expr: null});
    }
    SS.rows.push( row );
  }
})();

// because column names are A-Z
// A or a to 1
SS.letter_to_number = function ( azAZ ) {
	return azAZ.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0) + 1;
};
if (SS.letter_to_number("A") !== 1)
	throw "A => 1 failed";

// 1 to A; 26 to Z
SS.num_to_AZ = function (num) {
	return this.num_to_az(num).toUpperCase();
},

SS.num_to_az = function (num) {
	return 'abcdefghijklmnopqrstuvwxyz'.charAt(num-1); 
};

// for navigation, need to convert c5 into col 3 row 5
// D23 => ["D", "23"]
SS.separate_letters_numerics = function (str, letters, numerics) {
 if (0 < str.length) {
    var ch = str.charAt(0);
    if ('0' <= ch && ch <= '9')
        numerics.push( ch );
    else
        letters.push( ch );
    return this.separate_letters_numerics(str.substr(1), letters, numerics);
 }
 return [letters.join(""), numerics.join("")];
};

SS.is_cell_name = function (txt) {
  var re = /^([a-zA-Z]+)(\d+)$/ig;//looking for A2, z30, and such
  return re.exec( txt );
};

// d5 => {col:4, row:5}
SS.cell_id_for_cell_name = function ( aZ09 ) {
  var letters_numerics = this.separate_letters_numerics( aZ09, [], []);

  return {
     col: SS.letter_to_number( letters_numerics[0] ),
     row: parseInt( letters_numerics[1], 10)
  };
};

SS.is_valid_cell_id = function (new_id) {
   
   if (new_id.col < 1 || new_id.row < 1)
      return false
   if (this.COL_SIZE < new_id.col || this.ROW_SIZE < new_id.row)
      return false;
      
   return true;
};

SS.change_selection_if_valid = function (new_id) {
   if (!this.is_valid_cell_id(new_id))
      throw "Cell out of bounds!";
      
   this.selected_cell_id = new_id;
   return true;
};

// end of navigation related

// needed to put into cell
// 1 based to 0 based counting
SS.cell_for_ID = function (cell_id) {
  return this.rows[ cell_id.row - 1 ][ cell_id.col -1 ];
};

SS.cell_value = function (col, row) {
 return this.cell_for_ID({row:row, col:col})['val'];
};

SS.set_cell_expr = function (expr, id) {
  //log("actually setting "+expr+" into ("+id.row+","+id.col+")");

  var row = id.row;
  var col = id.col;
  this.cell_for_ID(id)['expr'] = expr;
};

SS.set_cell_value = function (val, id) {
  //log("finally set value "+val+" into ("+id.row+","+id.col+")");
  this.cell_for_ID(id)['val'] = val;
};


SS.put_formula_in_cell = function (formula, id) {
  log("convert "+formula+" into ("+id.col+","+id.row+")");
  //return put_expr(pin_down(formula, id), id);
  this.set_cell_expr(formula, id);
  this.set_cell_value(formula, id);
};
// end of put related

SS.actions = {
  select: function (cell_name) {
    SS.change_selection_if_valid( SS.cell_id_for_cell_name( cell_name ) );
  },
  left: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        col: (SS.selected_cell_id.col-delta),
        row: SS.selected_cell_id.row
     }
     return SS.change_selection_if_valid( new_id );
  },
  right: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        col: (SS.selected_cell_id.col+delta),
        row: SS.selected_cell_id.row
     }
     return SS.change_selection_if_valid( new_id );
  },
  up: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        row: (SS.selected_cell_id.row-delta),
        col: SS.selected_cell_id.col
     }
     return SS.change_selection_if_valid( new_id );
  },
  down: function (delta) {
  	 delta = delta ? parseInt(delta, 10) : 1;
     var new_id = {
        row: (SS.selected_cell_id.row+delta),
        col: SS.selected_cell_id.col
     }
     return SS.change_selection_if_valid( new_id );
  },
  h: function (delta) {
     return this.left(delta);
  },
  j: function (delta) {
     return this.down(delta);
  },
  k: function (delta) {
     return this.up(delta);
  },
  l: function (delta) {
     return this.right(delta);
  },
  put: function (formula, where) {     
     if (!where) {
        return SS.put_formula_in_cell( formula, SS.selected_cell_id );
     }
     if (SS.is_cell_name( where )) {
        return SS.put_formula_in_cell( formula, SS.cell_id_for_cell_name( where ) );
     }
  },//end of put action
  tmp: function (arg) {
  },
  etc: null 
};

return SS;
})(30, 26);