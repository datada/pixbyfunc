var parse_phone = function () {
  var text;
  var at;
  var ch;
  var error = function (m) {
    throw {
      name: "SyntaxError",
      message: m,
      at: at,
      text: text
    };
  };
  
  var next = function (c) {
    if (c && c != ch) {
      error("Expected '"+ c + "' instead of '"+ ch + "'");
    }
    ch = text.charAt(at);
    at += 1;
    return ch;
  };
  
  var white = function () {
    
    while (ch && ch <= ' ') {
      next();
    }
  };
  
  var digits = function (size) {
    var s = '';
    
    while ('0' <= ch && ch <= '9' && s.length < size) {
      s += ch;
      next();
    }
    
    return s;
  };
  
  //whatever is left over
  // save if digit
  // discard otherwise
  var extension = function () {
    var s = '';
    while (ch) {
      if ('0' <= ch && ch <= '9') {
        s += ch;
      }
      next();
    }
    return s;
  };
  
  var value = function () {
    var area = '';
    var three = '';
    var four = '';
    var ext = '';
    
    white();
    
    if ('1' === ch) {
      next('1');
      white();
    }
    
    if ('(' === ch) {
      next('(');
      white();
    }
    
    if ('.' === ch) {
      next('.');
      white();
    }
    
    area = digits(3);

    if (' ' === ch) {
      white();
    }
    if ('.' === ch) {
      next('.');
    }
    if (')' === ch) {
      next(')');
    }
    if (' ' === ch) {
      white();
    }
    
    three = digits(3);
    
    if (' ' === ch) {
      white();
    }
    if ('.' === ch) {
      next('.');
    }
    if ('-' === ch) {
      next('-');
    }
    
    four = digits(4);
    
    ext = extension();
    
    if (area.length < 3 || three.length < 3 || four.length < 4) {
      error("Syntax error");
    }
    
    var phonenumber = "(" + area + ") " + three + "-" + four;
    if (ext) {
      phonenumber += " x"+ ext;
    }
    return phonenumber;
  };
  
  
  return function (source) {
    
    var result;
    
    text = source;
    at = 0;
    ch = ' ';
    
    result = value();
    
    return result;
  };
}();//closure