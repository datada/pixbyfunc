var Q = function (J) {
  
  var qJSON = function (q) { 
    q = q ? q : {instruction:"what is ?", cs:[]};
    return JSON.stringify(q);
  };
  var handlers = {
    qs: function (args) {
       J("top");
       J("down qs");
       return J("json");
    },
    q: function (args) {
       this.qs(args);
       if (1 === args.length) {
         J("add {}");
         J("last");
         J('add instruction "what is?"');
         J('add rid ' + Math.random());
         J("add cs []");
         return J("json");
       }     
       if ('last' === args[1]) {
         return J("last");
       }
       return J("down "+(parseInt(args[1],10) - 1));
    },
    "?": function (args) {
      return "Unknown cmd: "+args[0];
    }
  };
  
  // will receive args from the cmd line
  return function (cmd) {
    var args = cmd.split(' ');
    if ( handlers[ args[0] ] ) {
      try {
        return handlers[ args[0] ]( args );
      } catch (e) {
        return "bad news: "+e;
      }
    }
    if (J) return J( cmd );
    return handlers["?"]( args );
  };
};
