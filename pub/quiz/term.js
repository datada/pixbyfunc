// uses modified/adopted/usurped code originally from http://www.genome.ist.i.kyoto-u.ac.jp/~ichinose/jsterm/
var Term = (function () {
    var buffer = [];
    var stdin;
    var log;
    var evaluate;
    
    var getKey = function (evt) {
        //log("keyCode="+evt.keyCode+" charCode="+evt.charCode, "info", "KeyCode");
        //Firefox always gives 0 keyCode on keypress (from keyCode vs charCode http://www.quirksmode.org/js/keys.html)
        return (evt.keyCode === 0) ? evt.charCode : evt.keyCode;  
    };

    //what gets assigned to var REPL by this js file
    return {
        init: function (el, fn, logger) {
            log = logger;
            stdin = el;//dom el such as text input or text area
            evaluate = fn;
            // YUI has var kl2 = new YAHOO.util.KeyListener(document, { ctrl:true, keys:89 }, 
            if(document.addEventListener){
                log("W3C", "info", "InitTerminal");
                stdin.addEventListener("keypress",this.read, true);
            }
            else if(document.attachEvent){
                log("IE", "info", "InitTerminal");
                stdin.attachEvent("onkeypress",this.read);
                stdin.attachEvent("onkeydown",this.read);
            }
            
            return true;
        },
        read: function (evt) {
            var key = getKey(evt);
            var ch = String.fromCharCode(key);
            var line; 
            if ("keypress" === evt.type && 13 === key) {//13 is enter or return
                //log(stdin.value, "info", "Evaluate");
                evaluate(stdin.value);
                stdin.value ="";
            }
        },
        etc: function (s) {
        }
    };
})();