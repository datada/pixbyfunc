var DB = { 
   tables: {},
   sql_handlers: {},
   utils: {}
};

DB.process = function ( sqls ) {
   return $.map(sqls, function (sql) {
      if (sql) {
        var tokens = $.trim(sql).split(" ");
        return DB.sql_handlers[ tokens[0] ]( tokens.slice(1), sql );
      }
      return null;
   });
};

DB.utils.remove_parens = function (s) {
    return DB.utils.undo_enclose("(", s, ")");
};

DB.utils.undo_enclose = function (front, s, end) {
   var str = new String(s);
   str = str.substr(str.indexOf(front)+1);
   str = str.substring(0, str.lastIndexOf(end));
   return str;
};

DB.utils.unquote = function (s) {
   return DB.utils.undo_enclose("'", s, "'");
};

// relation looks a bit different that internal tables for now
// {
//   cols:[c1, c1],
//   rows:[ {c1:v1,...},...]
// }
DB.utils.table_as_relation = function ( name, aka ) {
  var table = DB.tables[ name ];
  var cols = $.map(table.cols, function (col) {
    if (aka) {
      return aka+"."+col;
    }
    return name+"."+col;
  });
  var rows = $.map(table.rows, function (row) {
    var named_row = {};
    $.each(cols, function (c, col) {
      named_row[ col ] = row[ c ];
    });
    return named_row;
  });
  
  return {
    cols:cols,
    rows:rows
  };
};


DB.sql_handlers['create'] = function ( tokens, sql ) {
   var create_what = {};
   create_what['table'] = function ( tokens ) {
      
      //parse TableName(col type, col type, ....)
      var clause = tokens.join(" ");
      var indexOfLeftParen = clause.indexOf("(");
      var table_name = clause.substring(0, indexOfLeftParen);
      var column_names = clause.substr(indexOfLeftParen);
      log("create table "+table_name);
      var table = {rows:[]};
      
      //rid of ( )
      column_names = $.map(DB.utils.remove_parens(column_names).split(","), function (name_type, k) {
         return $.trim(name_type).split(" ")[0];//e.g. name text or id int
      });
      table.cols = column_names;
      
      DB.tables[ table_name ]= table ;
      return table;
   };
   
   if (create_what[ tokens[0] ]) {
      return create_what[ tokens[0] ]( tokens.slice(1) );
   }
};

DB.sql_handlers['insert'] = function ( tokens, sql ) {
   
   var is_numeric = function (n) {
     return !isNaN(parseFloat(n)) && isFinite(n);
   };
    
   var table_name = tokens[1]; //'into table-name values (...)'
   var values_str = DB.utils.remove_parens( tokens.slice(3).join(" ") );
   
   log("insert into "+table_name);
   log("values: "+values_str);
   
   var values = $.map(values_str.split(","), function (val, k) {
      if (is_numeric(val)) {
         return parseFloat(val);
      } 
      return DB.utils.unquote($.trim(val));
   });

   DB.tables[ table_name ].rows.push( values );
   return 1;
};

DB.select = function (col_names) {
  
  var cols = $.map(col_names.split(','), function (v) { return $.trim(v); });
  
  var tables = [];
  var conditions = '';
  
  var join_two = function (table1, table2) {

    return {
      cols: $.merge(table1.cols, table2.cols),
      rows: $.map(table1.rows, function (row1) {
        return $.map(table2.rows, function (row2) {
          var new_row = {};
          $.each(row1, function (k,v) {
            new_row[ k ] = v;
          });
          $.each(row2, function (k, v) {
            new_row[ k ] = v;//merge row1 with row2
          });
          return new_row;
        });
      })
    };
  };
  
  var join = function (relation, table_names) {
    
    $.each(table_names, function (k, tn) {
         relation = join_two(relation, DB.utils.table_as_relation( tn.name, tn.aka ));
    });
    
    return relation;
  };
  
  var filter = function (relation) {
    
    if ('' === conditions) {
      log("no sql where clause");
      return relation;
    }
    var context = relation;
    var sql_where_tokens = conditions.split(" ");
    var js_where_tokens = $.map(sql_where_tokens, function (token) {
      
      if ("and" === token.toLowerCase()) return "&&";

      if ("or" === token.toLowerCase()) return "||";

      if ("=" === token.toLowerCase()) return "===";

      if (-1 < context.cols.indexOf( token )) {
        return "row['"+token+"']";
      }
      
      return token;
    });
    
    
    var where_clause = js_where_tokens.join(" ");
    if ("" === $.trim(where_clause)) {
      log("no js where clause");
      return relation;
    }
    var rows = $.map(relation.rows, function (row) {
       
        if (!where_clause) {
          return row;
        }
        log( where_clause );
        if ( eval("("+ where_clause +")") ) {
          return row;
        }
        
        return null;
      
    });
    
    return {
       cols:relation.cols,
       rows:rows
     }
  };
  
  
  var project = function ( relation ) {
    
    var projected = cols;
    
    if ("*" === projected[0]) { 
      return relation;
    }
    
    //e.g. Student.*, Apply.*
    projected = $.map(projected, function (col) {
      if ( col.indexOf("*") === (col.length - 1) ) {
        var tbl_name = col.split(".")[0];
        return $.map(DB.tables[ tbl_name ].cols, function (col_name) {
          return tbl_name+"."+col_name;
        });
      }
      return col;
    });
    // unqualified names such as sName
    projected = $.map(projected, function (col) {
      if ( col.indexOf(".") < 0 ) {
        var possible_names =  $.map(relation.cols, function (col_name) {
          if (-1 < col_name.indexOf( col )) {
            return col_name;
          }
        });
        if (1 === possible_names.length) {
          return possible_names[0];
        }
        throw Error("Ambiguous column name "+col);
      }
      return col;
    });
    
    
    var narrowed_rows = $.map (relation.rows, function (row) {
      var narrowed = {};
      $.each(row, function (k,v) {
        
        if (projected.indexOf( k ) < 0) {
          log("do not want "+k);
        } else {
          narrowed[ k ] = v;
        }
      });
      return narrowed;
    });
    
    return { 
      cols: projected,
      rows: narrowed_rows
    };
  };
  

 
  return {
    from: function (table_names) { 
      tables = $.map(table_names.split(','), function (v) { 
        return $.trim(v); 
      });
      tables = $.map(tables, function (table) {
        if (-1 < table.indexOf(" ")) {//Table var such as Student S1
          var name_aka = $.map(table.split(" "), function (token) {
            if (token === "") {
              return null;
            }
            return token;
          });
          return {
            name: name_aka[0],
            aka: name_aka[1]
          };
        }
        return {
          name: table,
          aka: null
        };
      });
      
      return this; 
    },
    where: function (where_clause) { 
      conditions = where_clause;
      return this;
    },
    fetch: function () {
      var relation = DB.utils.table_as_relation( tables[0]['name'], tables[0].aka );
      return project(filter(join( relation, tables.slice(1) )));
    }
  };
};

DB.operate = function ( relation ) {
  return {
    out: function () { return relation; },
    change_col: function (old_name, new_name) {
      relation.cols[ relation.cols.indexOf(old_name) ] = new_name;
      relation.rows = $.map(relation.rows, function (row) {
        row[ new_name ] = row[ old_name ];
        delete row[ old_name ];
        return row;
      });
      return this;
    },
    add_col: function (name, proc) {
      relation.cols.push( name );
      relation.rows = $.map(relation.rows, function (row) {
        row[ name ] = proc( row );
        return row;
      });
      return this;
    },
    filter: function (proc) {
      relation.rows = $.map(relation.rows, function (row) {
        if (proc(row)) { 
          return row;
        }
        return null;
      });
      return this;
    },
    union: function ( another ) {
      if(!another) throw Error(" cannot make union with null relation");
      $.each(another.rows, function (k,v) {
        relation.rows.push( v );
      });
      return this;
    },
    intersect: function ( another ) {
      var commons = $.map(relation.rows, function (row) {
        if (DB.utils.membership(row, another.rows)) {
          return row;
        }
        return null;
      });
      
      relation.rows = commons;
      return this;
    },
    except: function ( another ) {
      
      var uniques = $.map(relation.rows, function (row) {
        if (DB.utils.membership(row, another.rows)) {
          return null;
        }
        return row;
      });
      
      relation.rows = uniques;
      return this;
    },
    distinct: function () {
      
      var uniqs = [];
      $.each(relation.rows, function (r, row) {
        if (DB.utils.membership(row, uniqs)) {
        } else {
          uniqs.push( row );
        }
      });
      
      relation.rows = uniqs;
      return this;
    },
    order_by: function (sorter) {
      relation.rows.sort( sorter );
      return this;
    }
  };
};

DB.utils.membership = function ( needle, rows ) {

  var matches = $.map(rows, function (row) {
    var match_found = true;
    $.each(row, function (k, v) {
      if (v !== needle[k]) { 
        match_found = false;
      }
    });
    if (match_found) {
      return row;
    }
    return null;
  });
  
  return (0 < matches.length);
};

var test = function () {
  var r1 = DB.select('Student.sID, sName, GPA, Apply.cName, enrollment').from('Student, College, Apply').where("Apply.sID = Student.sID and Apply.cName = College.cName").fetch();

  var sorter = function (attr, desc) {
    var flag = desc ? -1 : 1;
    return function (a, b) {
      if (a[attr] < b[attr]) return -1 * flag;
      if (a[attr] > b[attr]) return 1 * flag;
      return 0; 
    }
  };
  
  return DB.operate( r1 ).order_by( sorter('Student.GPA', 'desc') ).out();
};
