var DB = { 
   tables: {},
   actions: {},
   utils: {}
};

DB.process = function ( sqls ) {
   return $.map(sqls, function (sql, i) {
      if (sql) {
        var tokens = $.trim(sql).split(" ");
        if (DB.actions[ tokens[0] ]) {
           return DB.actions[ tokens[0] ]( tokens.slice(1), sql );
        }
        log("not finding action "+tokens[0]);
      }

      return null;
   });
};

DB.utils.remove_parens = function (s) {
      return DB.utils.undo_enclose("(", s, ")");
};

DB.utils.undo_enclose = function (front, s, end) {
   var str = new String(s);
   str = str.substr(str.indexOf(front)+1);
   str = str.substring(0, str.lastIndexOf(end));
   return str;
};

DB.utils.unquote = function (s) {
   return DB.utils.undo_enclose("'", s, "'");
};

DB.utils.table_as_relation = function ( name ) {
  var table = DB.tables[ name ];
  var cols = $.map(table.cols, function (col) {
    return name+"."+col;
  });
  var rows = $.map(table.rows, function (row) {
    var named_row = {};
    $.each(cols, function (c, col) {
      named_row[ col ] = row[ c ];
    });
    return named_row;
  });
  
  return {
    cols:cols,
    rows:rows
  };
};



DB.actions['create'] = function ( tokens, sql ) {
   var create_what = {};
   create_what['table'] = function ( tokens ) {
      
      //parse TableName(col type, col type, ....)
      var clause = tokens.join(" ");
      var indexOfLeftParen = clause.indexOf("(");
      var table_name = clause.substring(0, indexOfLeftParen);
      var column_names = clause.substr(indexOfLeftParen);
      log("create table "+table_name);
      var table = {rows:[]};
      
      //rid of ( )
      column_names = $.map(DB.utils.remove_parens(column_names).split(","), function (name_type, k) {
         return $.trim(name_type).split(" ")[0];//e.g. name text or id int
      });
      table.cols = column_names;
      
      DB.tables[ table_name ]= table ;
      return "Created TABLE "+table_name;
   };
   if (create_what[ tokens[0] ]) {
      create_what[ tokens[0] ]( tokens.slice(1) );
   }
};

DB.actions['insert'] = function ( tokens, sql ) {
   
   var is_numeric = function (n) {
     return !isNaN(parseFloat(n)) && isFinite(n);
   };
   
   log( tokens.join(" "));
   
   var table_name = tokens[1]; //'into table-name values (...)'
   var values_str = DB.utils.remove_parens( tokens.slice(3).join(" ") );
   
   log("insert into "+table_name);
   log("values: "+values_str);
   
   var values = $.map(values_str.split(","), function (val, k) {
      if (is_numeric(val)) {
         return parseFloat(val);
      } 
      return DB.utils.unquote($.trim(val));
   });

   DB.tables[ table_name ].rows.push( values );
   return 1;
};

DB.utils.parse_select_tokens = function (sql_tokens) {
   
   var cols_tokens = [];
   var tables_tokens = [];
   var where_tokens = [];
   var before_FROM = true;
   var before_WHERE = true;
   
   $.each(sql_tokens, function (i, token) {
      if ("FROM" === $.trim(token).toUpperCase()) {
         before_FROM = false;
      } else if ("WHERE" === $.trim(token).toUpperCase()) {
          before_WHERE = false;
      } else if (before_FROM) {
         cols_tokens.push( token );
      } else if (before_WHERE) {
         tables_tokens.push( token );
      } else {
         where_tokens.push (token);
      }
   });
   
   return { 
     select:$.map(cols_tokens.join(" ").split(","), function (token) { return $.trim(token);}), 
     from:$.map(tables_tokens.join(" ").split(","), function (token) { return $.trim(token);}), 
     where:where_tokens 
   };
};


DB.utils.expand_where = function (parsed, context) {
  
    parsed.where = $.map(parsed.where, function (token) {
      
      if ("and" === token.toLowerCase()) return "&&";

      if ("or" === token.toLowerCase()) return "||";

      if ("=" === token.toLowerCase()) return "===";

      if (-1 < context.cols.indexOf( token )) {
        return "row['"+token+"']";
      }
      
      return token;
    });
    

    return {
      select: parsed.select,
      from: parsed.from,
      where: parsed.where
    };

};

DB.utils.expand_select = function (parsed) {
   
    var table_name = parsed.from[0];
    //handle naked * for now
    // [*] => [table.col, ...]
    if ("*" === parsed.select[0]) { 
      parsed.select = $.map(DB.tables[ table_name ].cols, function (col_name) {
        return table_name+"."+col_name;
      });
    }
    
    var sql = "select "+parsed.select.join(", ")+" from "+ parsed.from.join(", ");
    if (0 < parsed.where.length) {
      sql += " where "+parsed.where.join(" ");
    }
    
    return {
      sql:sql,
      select: parsed.select,
      from: parsed.from,
      where: parsed.where
    };
};


DB.actions['select'] = function ( tokens, sql ) {
   
   var join_two = function (table1, table2) {
     console.warn(JSON.stringify(table1)); 
     console.warn(JSON.stringify(table2));
     return {
       cols: $.merge(table1.cols, table2.cols),
       rows: $.map(table1.rows, function (row1) {
         return $.map(table2.rows, function (row2) {
           var new_row = {};
           $.each(row1, function (k,v) {
             new_row[ k ] = v;
           });
           $.each(row2, function (k, v) {
             new_row[ k ] = v;//merge row1 with row2
           });
           return new_row;
         });
       })
     };
   };
   
   var join = function (table_names, relation, parsed) {
     
     $.each(table_names, function (k, table_name) {
       if (0<k) {
          relation = join_two(relation, DB.utils.table_as_relation( table_name ));
       }
     });
     
     return {
       relation:relation,
       select:parsed.select,
       from:parsed.from,
       where:parsed.where
     };
   };
   
   var filter = function (parsed) {
     
     var relation = parsed.relation;
     
     parsed = DB.utils.expand_where(parsed, relation);
     
     var where_tokens = parsed.where;
     
     var where_clause = where_tokens.join(" ");
     var rows = $.map(relation.rows, function (row) {
        
         if (!where_clause) {
           return row;
         }
         console.warn( where_clause );
         if ( eval("("+ where_clause +")") ) {
           console.warn("truty was "+where_clause);
           return row;
         }
         
         console.warn("falsy was "+where_clause);
         console.warn("in the context "+JSON.stringify(row))
         return null;
       
     });
     
     return {
       relation: {
         cols:relation.cols,
         rows:rows
       },
       select:parsed.select,
       from:parsed.from,
       where:parsed.where
     };
   };

   var project = function ( parsed ) {
     
     var relation = parsed.relation;
     parsed = DB.utils.expand_select(parsed);
     var projected = parsed.select;
     
     log("wanted ");
     log(projected);
     var narrowed_rows = $.map (relation.rows, function (row) {
       var narrowed = {};
       $.each(row, function (k,v) {
         if (projected.indexOf( k ) < 0) {
           log("do not want "+k);
           
         } else {
           log("want "+k);
           narrowed[ k ] = v;
         }
       });
       return narrowed;
     });
     
     return { 
       relation:{ 
       cols: projected,
       rows: narrowed_rows},
       select: parsed.select,
       from: parsed.from,
       where: parsed.where
    };
   };
   
   var parsed = DB.utils.parse_select_tokens( tokens );
   var relation = DB.utils.table_as_relation( parsed.from[0] );
   
   return project( filter(  join(parsed.from, relation, parsed)))['relation'];
};
