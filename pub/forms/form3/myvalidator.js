var Validation = (function() {
    
    var defaultLoc = "en_US";

    var rules = {

        email: {
           check: function (value) {

               if (value) {
                   // minimal email looks like A@b.c
                   if ( value.length < 5 ) {   return false; }
                   if ( value.indexOf("@") < 1 ) { return false; }
                   if ( value.indexOf(".") < 3 ) { return false; }
                   
               } else {
                   return false;
               }
               return true;
           },
           msg: function (loc) {
               
               loc = loc ? loc : defaultLoc;
               
               return {
                   en_US: "Invalid format.",
                   en_CA: "Invalid format",
                   fr_CA: "Invalid format"
               }[loc];
           } 
        },//rule
        
        password: {
            check: function (value) {
                
                if (value) {
                    if (value.length < 6 ) { return false; }
                } else {
                    return false;
                }
                return true;
            },
            msg: function (loc) {
                loc = loc ? loc : defaultLoc;

                return {
                    en_US: "Too short.",
                    en_CA: "Too short.",
                    fr_CA: "Too short."
                }[loc];
            }
        }, //rule
        
        password_confirm: {
            check: function (p1, p2) {
                
                if (p2 && (p2 === p1)) {
                    return true;
                } else {
                    return false;
                }
                return true;
            },
            msg: function (loc) {
                
                loc = loc ? loc : defaultLoc;

                return {
                    en_US: "Must match password.",
                    en_CA: "Must match password.",
                    fr_CA: "Must match password.(?)"
                }[loc];
            }
        },//rule
        
        required: {

           check: function(value) {

               if (value) {
                   return true;  
               } else {
                   return false;
               }
        
               return true;
           },
           msg : function (loc) {
               
               loc = loc ? loc : defaultLoc;
               
               return {
                   en_US: "This field is required.",
                   en_CA: "This field is required.",
                   fr_CA: "This field is required.(?)"
               }[loc];
           }
        }//rule
    }//rules
    
    //public methods
    return {

        addRule : function(name, rule) {

            rules[name] = rule;
        },
        getRule : function(name) {

            return rules[name];
        }
    };//return
})();//Validation closure is defined and invoked
