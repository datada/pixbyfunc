
angular.module('project', []).
  config(function($routeProvider) {
    $routeProvider.
      when('/', {controller:ListCtrl, templateUrl:'list5.html'}).
      when('/edit/:projectId', {controller:EditCtrl, templateUrl:'detail5.html'}).
      when('/new', {controller:CreateCtrl, templateUrl:'detail5.html'}).
      otherwise({redirectTo:'/'});
  });


var data = [
  {id: 1, name: "jQuery", description: "Master of DOM", site:"http://jquery.com"},
  {id: 2, name: "Ember.js", description: "Beat angularjs!", site:"http://emberjs.org"}
];
 
var ListCtrl = function  ($scope) {

  $scope.projects = data;
  $scope.orderProp = "name";
};
 
 
var CreateCtrl = function ($scope, $location) {
  console.log("CreateCtrl called");
  $scope.save = function() {
    var ids = _.map(data, function(d){ return d.id; });
    console.log("ids "+ids);
    var id = _.max(ids) + 1;
    $scope.project["id"] = id;
    data.push($scope.project);
    $location.path('/edit/' + id);
  };
};
 
 
var EditCtrl = function ($scope, $location, $routeParams) {
  console.log("EditCtrl called");
  var self = this;
  console.log("looking for "+$routeParams.projectId)
  var project = _.findWhere(data, {id: parseInt($routeParams.projectId, 10)});
  self.original = project;

  $scope.project = {
    id: self.original["id"],
    name: self.original["name"],
    site: self.original["site"],
    description: self.original["description"]
  };

  $scope.isClean = function() {
    return angular.equals(self.original, $scope.project);
  }
 
  $scope.destroy = function() {
    $scope.projects = _.reject(data , function(project){ return self.original["id"] === project["id"]; });
    $location.path('/list');
  };
 
  $scope.save = function() {
    self.original["id"] = $scope.project["id"];
    self.original["name"] = $scope.project["name"];
    self.original["description"] = $scope.project["description"];

    $location.path('/');
  };
};
