'use strict';

angular.module('elementsrus', ['elementsrus.filters', 'elementsrus.services']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/elements', {templateUrl: 'partials/element-list.html', controller: ElementListCtrl});
    $routeProvider.when('/elements/:elementNumber', {templateUrl: 'partials/element-detail.html', controller: ElementDetailCtrl});
    $routeProvider.when('/cart', {templateUrl: 'partials/cart.html', controller: CartCtrl});
    $routeProvider.otherwise({redirectTo: '/elements'});
  }]);
