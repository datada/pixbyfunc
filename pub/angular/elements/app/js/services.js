'use strict';


angular.module('elementsrus.services', ['ngResource']).
    factory('Element', function($resource){
  		return $resource('elements/:elementNumber.json', {}, {
    		query: {method:'GET', params:{elementNumber:'elements'}, 
    		isArray:true}});
	}).
	factory('CartCache', function ($cacheFactory) {
		return $cacheFactory('CartCache');
	}).
	value('version', '0.1');