'use strict';


var ElementListCtrl = function ($scope, Element) {
  
  $scope.elements = Element.query();
  $scope.orderProp = "weight";
};


var ElementDetailCtrl = function ($scope, $routeParams, Element, CartCache) {

  $scope.element = Element.get({elementNumber: $routeParams.elementNumber}, function(element) {
    $scope.mainImageUrl = (element.images && 0 < element.images.length) ? element.images[0] : "";
  });

  $scope.quantity = "0";

  $scope.setImage = function (imageUrl) {
    $scope.mainImageUrl = imageUrl;
  };

  $scope.addToCart = function () {
    if ("0" === $scope.quantity) {
    } else {
      var items = CartCache.get('items');

      items = items ? items : [];

      items.push({
      "name": $scope.element.name,
      "number": $scope.element.number, 
      "symbol": $scope.element.symbol, 
      "unit": $scope.element.unit,
      "price": $scope.element.price,
      "currency": $scope.element.currency,
      "quantity": parseFloat($scope.quantity),
      "remove": false
      });
      CartCache.put('items', items);
      $scope.quantity = "0";
    }
  };
};

var CartCtrl = function ($scope, CartCache) {
  var items = CartCache.get('items');
  $scope.items = items ? items : [];

  $scope.total = function () {
    var sum = _.reduce($scope.items, function(memo, item){ return memo + (item.quantity * item.price); }, 0);
    return sum;
  };

  $scope.removeItems = function () {
    //console.log("remove items")
    var items = _.filter($scope.items, function (item) {
      console.log(item.remove);
      return !item.remove;
    });
    $scope.items = items;
    CartCache.put('items', items);
  };

};
 