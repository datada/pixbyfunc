from pandas import DataFrame
import pandas as pd 

import csv, json

def create_merged_list():
    df1 = pd.read_csv("elements-list.1.csv")
    df2 = pd.read_csv("elements-list.2.csv")

    cols= ["number", "origin of name", "melting point (k)", "boiling point (k)", "Heat", "neg", "abundance (mg/kg)"]
    df2a = df2[cols]

    df_merged = pd.merge(df1, df2a, on="number")
    df_merged.to_csv("elements-list.1+2.csv")

def num_if_so (s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except:
            return s

def numberfy(xs):
    return [ num_if_so(x) for x in xs]

def create_json():
    with open("elements-list.1+2.csv") as f:
        lines = list(csv.reader(f))
        header, vals = lines[0], lines[1:]
        rows = [dict(zip(header, numberfy(val))) for val in vals]

        for row in rows:
            row["snippet"] = "not in stock"
            row["instock"] = False

        with open("elements-list.json", "w") as f:
            f.write(json.dumps(rows))

# cat elements-list.json | python -mjson.tool > elements.json
# manually edit elements.json


def create_ind_jsons():
    with open("elements.json") as f:
        elements = json.load(f)

        for el in elements:
            # print json.dumps(el)            
            with open("{0}.json".format(el["number"]), "w") as f2:
                f2.write(json.dumps(el))


if __name__ == '__main__':
    # create_ind_jsons()
    print "done"