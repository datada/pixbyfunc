'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

// describe('my app', function() {

//   beforeEach(function() {
//     browser().navigateTo('../../app/index.html');
//   });


//   it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
//     expect(browser().location().url()).toBe("/view1");
//   });


//   describe('view1', function() {

//     beforeEach(function() {
//       browser().navigateTo('#/view1');
//     });


//     it('should render view1 when user navigates to /view1', function() {
//       expect(element('[ng-view] p:first').text()).
//         toMatch(/partial for view 1/);
//     });

//   });


//   describe('view2', function() {

//     beforeEach(function() {
//       browser().navigateTo('#/view2');
//     });


//     it('should render view2 when user navigates to /view2', function() {
//       expect(element('[ng-view] p:first').text()).
//         toMatch(/partial for view 2/);
//     });

//   });
// });

describe('Elements R Us App', function() {
 
  describe('Element list view', function() {
 
    beforeEach(function() {
      browser().navigateTo('../../app/index.html');
    });
 
 
    it('should filter the element list as user types into the search box', function() {
      expect(repeater('.elements div').count()).toBe(109);
 
      input('query').enter('lithium');
      expect(repeater('.elements div').count()).toBe(1);
 
      input('query').enter('hydro');
      expect(repeater('.elements div').count()).toBe(1);
    });

    it('should be possible to control element order via the drop down select box',
        function() {
 
      select('orderProp').option('Name');
 
      expect(repeater('.elements div', 'Element List').column('element.name')).
          toEqual(["Actinium","Aluminum","Americium","Antimony","Argon","Arsenic","Astatine","Barium","Berkelium","Beryllium","Bismuth","Bohrium","Boron","Bromine","Cadmium","Calcium","Californium","Carbon","Cerium","Cesium","Chlorine","Chromium","Cobalt","Copper","Curium","Dubnium","Dysprosium","Einsteinium","Erbium","Europium","Fermium","Fluorine","Francium","Gadolinium","Gallium","Germanium","Gold","Hafnium","Hassium","Helium","Holmium","Hydrogen","Indium","Iodine","Iridium","Iron","Krypton","Lanthanum","Lawrencium","Lead","Lithium","Lutetium","Magnesium","Manganese","Meitnerium","Mendelevium","Mercury","Molybdenum","Neodymium","Neon","Neptunium","Nickel","Niobium","Nitrogen","Nobelium","Osmium","Oxygen","Palladium","Phosphorus","Platinum","Plutonium","Polonium","Potassium","Praseodymium","Promethium","Protactinium","Radium","Radon","Rhenium","Rhodium","Rubidium","Ruthenium","Rutherfordium","Samarium","Scandium","Seaborgium","Selenium","Silicon","Silver","Sodium","Strontium","Sulfur","Tantalum","Technetium","Tellurium","Terbium","Thallium","Thorium","Thulium","Tin","Titanium","Tungsten","Uranium","Vanadium","Xenon","Ytterbium","Yttrium","Zinc","Zirconium"]);
    });

    it('should render element specific links', function () {
      input('query').enter('Hydrogen');
      element('.elements div p a').click();
      expect(browser().location().url()).toBe('/elements/1');
    });

  });

  describe('Element detail view', function () {
    beforeEach(function () {
      browser().navigateTo('../../app/index.html#/elements/2');
    });

    it('should display Helium', function () {
      expect(binding('element.name')).toBe('Helium');
    });


    it('should display the first phone image as the main phone image', function() {
      expect(element('img.element').attr('src')).toBe('img/elements/helium.0.jpg');
    });
 

    it('should swap main image if a thumbnail image is clicked on', function() {
      element('.element-thumbs li:nth-child(3) img').click();
      expect(element('img.element').attr('src')).toBe('img/elements/helium.2.jpg');
 
      element('.element-thumbs li:nth-child(1) img').click();
      expect(element('img.element').attr('src')).toBe('img/elements/helium.0.jpg');
    });

  });
});
