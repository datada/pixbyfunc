window.storage = (function () {
   return {
      loadThings: function (ownerId, good, bad) {
         
         var req = $.ajax({
           type: "GET",
           url: "/thing/?ownerId="+ownerId,
           dataType: "json" //JSPN.parse is invoked
        });
        req.done(function (obj) {
           //obj = {"results":[...]}
           return good( obj["results"] );
        });
        req.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
      },
      saveAllThings: function (things) {
/*         _.each(things, function (thing) {
            thing.ownerId = thing.ownerId ? thing.ownerId : window.ENV.ownerId;
            thing.what = thing.what  ? thing.what :'something';
            localStorage.setItem( thing.objectId, JSON.stringify(thing) );
         });*/
      },
      updateThing: function (thing, good, bad) {
         var _obj = {};
         _obj['REST']= 'PUT';
         _obj['content'] = JSON.stringify( thing );//compressing
   
         var req =  $.ajax({
           type: "POST",
           url: "/thing/"+thing.objectId,//hint to put or delete
           data: _obj,
           dataType: "json" //JSON.parse invoked
         });
         req.done(function (obj) {
            //obj = {"updatedAt": "" }
            return good( obj );
         });
         req.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
         });
      },
      createThing: function (thing, good, bad) {
     
           var _obj = {};
           _obj['what']= thing.what;
           _obj['ownerId'] = thing.ownerId ? thing.ownerId : window.ENV.ownerId;
           _obj['content'] = JSON.stringify( thing );//compressing
     
           var req =  $.ajax({
             type: "POST",
             url: "/thing/",

             data: _obj,
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
              //obj = {"createdAt": "",  "objectId": "abc"}
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });        
        },
        deleteThing: function (thing, good, bad) {
           var _obj = {};
           _obj['REST']= 'DELETE';

           var req =  $.ajax({
             type: "POST",
             url: "/thing/"+thing.objectId,//hint to put or delete
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
              //obj = {"updatedAt": "" }
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });
        }
      };
})();