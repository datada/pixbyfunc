window.storage = (function () {
   return {
      loadThings: function (ownerId, callback) {
         var i, thing, things=[], max = localStorage.length;
         for (i=0; i<max; i+=1) {
            thing = JSON.parse(localStorage.getItem( localStorage.key(i) ));
            thing.objectId = localStorage.key(i);
            if (thing.ownerId === ownerId) things.push( thing );
         }
         return callback(things);
      },
      saveAllThings: function (things) {
         _.each(things, function (thing) {
            console.log("saving");
            thing.ownerId = thing.ownerId ? thing.ownerId : window.ENV.ownerId;
            thing.what = thing.what  ? thing.what :'something';
            localStorage.setItem( "oid"+Math.random(), JSON.stringify(thing) );
         });
      }
   };
})();