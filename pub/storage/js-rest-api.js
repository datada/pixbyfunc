window.storage = (function () {
   return {
      loadThings: function (oid, good, bad) {
         
         var req = $.ajax({
           type: "GET",
           url: "https://api.parse.com/1/classes/QAThing",
           crossDomain: true,
           headers: {
              "X-Parse-Application-Id":  ENV['ApplicationId'],
              "X-Parse-REST-API-Key": ENV['ClientKey']
           },
           data: "where="+JSON.stringify({ownerId: oid}),
           dataType: "json" //JSPN.parse is invoked
        });
        req.done(function (obj) {
              //obj = {"results":[...]}
              return good( obj["results"] );
        });
        req.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
      },
      saveAllThings: function (things) {
/*         _.each(things, function (thing) {
            thing.ownerId = thing.ownerId ? thing.ownerId : window.ENV.ownerId;
            thing.what = thing.what  ? thing.what :'something';
            localStorage.setItem( thing.objectId, JSON.stringify(thing) );
         });*/
      },
      saveOneThing: function (thing) {
         
      },
      createThing: function (thing, good, bad) {
     
           var _obj = {};
           _obj['what']= thing.what;
           _obj['ownerId'] = thing.ownerId ? thing.ownerId : window.ENV.ownerId;
           _obj['content'] = JSON.stringify( thing );//adhoc....
     
           var req =  $.ajax({
             type: "POST",
             url: "https://api.parse.com/1/classes/QAThing",
             crossDomain: true,
             headers: {
                "X-Parse-Application-Id":  ENV['ApplicationId'],
                "X-Parse-REST-API-Key": ENV['ClientKey']
             },
             contentType: "application/json",
             data: JSON.stringify( _obj ),
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
                 //obj = {"createdAT": "objectId": }
                 return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });        
        },
        examplePost: function () {
          var xhr = new XMLHttpRequest();
          xhr.open("POST", "https://api.parse.com/1/classes/QAThing", true);
          xhr.setRequestHeader("X-Parse-Application-Id", ENV['ApplicationId']);
          xhr.setRequestHeader("X-Parse-REST-API-Key", ENV['ClientKey']);
          xhr.setRequestHeader("Content-Type", "application/json");
          xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
              var result = JSON.parse(xhr.responseText);
              if (result.objectId) {
                console.log("saved an object with id: " + result.objectId);
                return result.objectId;
              }
            }
          };
          var _obj = {};
          _obj[_key]= _val;
          var data = JSON.stringify( _obj );//nesting of JSON is a bit confusing
          xhr.send(data);
        },
        deleteThing: function (_key) {
           //
        }
      };
})();