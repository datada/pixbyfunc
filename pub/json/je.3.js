var J = function (init) {
  init = init ? init : {};  
  var it = {
    up:null, 
    content:init, 
    my_index: null
  };
  
  var typeOf = function (value) {
      var s = typeof value;
      if (s === 'object') {
          if (value) {
              if (typeof value.length === 'number' &&
                      !(value.propertyIsEnumerable('length')) &&
                      typeof value.splice === 'function') {
                  s = 'array';
              }
          } else {
              s = 'null';
          }
      }
      return s;
  };
  
  var handlers = {
    add: function (args) {
      var rest = [];
      if ('string' === typeOf(it.content)) {
        rest = args.slice(1);
        it.content += args.slice(1).join(" ");
      } else if ('number' === typeOf(it.content)) {
        rest = args.slice(1);
        while (rest.length) {
          it.content += JSON.parse(rest.pop());
        }
      } else {
        if (2 < args.length) {//add key val val val ....
          rest = args.slice(2);
          it.content[ args[1] ] = JSON.parse( rest.join(" ") );
        } else  {
          it.content.push( JSON.parse(args[1]) );//no key assume it's array and pushing a num or str
        }
      }
      

      return this.list( args );
    },
    end: function (args) {
        this.top( args );
	localStorage.setItem('tmp', JSON.stringify(it.content));
        window.open('view.html', '_blank');
        return this.list( args );
    },
    down: function (args) {
      if (args.length < 2) return "Where to ?";
      
      var where = it.content[ args[1] ];
      if ( where) {
        it = { 
          up:it,// so we can go back up
          content: where,
          my_index: args[1]
        };
        return this.list( args );
      }
      return "no such thing";
    },
    last: function (args) {
      if ('array' === typeOf(it.content)) {
        var i = it.content.length - 1; 
        return this.down(['last', i]);        
      }  
    },
    list: function (args) {
      return JSON.stringify( it.content );
    },
    map: function (args) {
      var m = function (ary, fn) {
        var vals = [];
        for (var i in ary) {
          vals.push( fn(ary[i], i) );
        }
        return vals;
      };
      var fn = "(function (v,k) { return "+
      args.slice(1).join(" ") + "; })";
      console.log(fn);
      fn = eval(fn);
      if ('function' === typeof fn) {
      it.content = m(it.content, fn);
      }
      return this.list( args );
    },
    swap: function (args) {
      var from = args[1];
      var to = args[2];
      if (it.content[from] && it.content[to]) {
        var tmp = it.content[ to ];
        it.content[ to ] = it.content[ from ];
        it.content[ from ] = tmp;
        return this.list( args );
      } else { return "not valid args";}  
    },
    rid: function ( args ) {
      if (1 < args.length) {
        delete it.content[ args[1] ];
      } else if ('array'===typeOf(it.content)) {
        //remove nulls
        var vals = [];
        for (var i in it.content) {
          if (it.content[i]) vals.push( it.content[i] );
        }
        it.content = vals;     
      }
      return this.list( args );
    },
    top: function (args) {
      while (it.up) {
        this.up( args );
      }
      return "at the top";
    },
    up: function (args) {
      if (it.up) {
        if (it.my_index) {
          it.up.content[ it.my_index ] = it.content;//save
          it.my_index = null;
        }
        it = it.up;
        return this.list( args );
      }
      return "at the top already";
    },
    val: function (args) {
	if (args.length < 2) return this.list( args );
        rest = args.slice(1);
        it.content = JSON.parse( rest.join(" ") );
        return this.list( args );
    },
    ver: function (args) {
      return this.version( args );
    },
    version: function (args) {
	return "0.3";
    },
    "?": function (args) {
      return "Unknown cmd: "+args[0];
    }
  };
  
  // will receive args from the cmd line
  return function (cmd) {
    var args = cmd.split(' ');
    if ( handlers[ args[0] ] ) {
      try {
        return handlers[ args[0] ]( args );
      } catch (e) {
        return "bad news: "+e;
      }
    }
    return handlers[ "?" ]( args );
  };
};
