window.storage = (function () {
   return {
      loadThings: function (ownerId, good, bad) {
         
         var req = $.ajax({
           type: "GET",
           url: "/thing/?ownerId="+ownerId,
           dataType: "json" //JSPN.parse is invoked
        });
        req.done(function (obj) {
           //obj = {"results":[...]}
           return good( obj["results"] );
        });
        req.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
      },
      createThing: function (thing, good, bad) {
     
           var _obj = {};
           _obj['thing'] = JSON.stringify( thing );//compressing
     
           var req =  $.ajax({
             type: "POST",
             url: "/thing/",
             data: _obj,
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
              //obj = {"createdAt": "",  "objectId": "abc"}
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });        
        },
        readThing: function (objectId, good, bad) {
            var req = $.ajax({
              type: "GET",
              url: "/thing/"+objectId,
              dataType: "json" //JSPN.parse is invoked
           });
           req.done(function (obj) {
              //obj = {"results":[...]}
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });
        },
        updateThing: function (thing, good, bad) {
           
           var objectId = thing.objectId;
           
           //keywords and some field not to be updated
           _.each(['createdAt', 'updatedAt', 'objectId', 'ownerId'], function (word) {
              delete thing[ word ];
           });
         
           var _obj = {};
           _obj['REST']= 'PUT';
           _obj['thing'] = JSON.stringify( thing );//compressing
           
           var req =  $.ajax({
             type: "POST",
             url: "/thing/"+objectId,//hint to put or delete
             data: _obj,
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
              //obj = {"updatedAt": "" }
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });
        },
        deleteThing: function (thing, good, bad) {
           
           var objectId = thing.objectId;
           
           var _obj = {};
           _obj['REST']= 'DELETE';

           var req =  $.ajax({
             type: "POST",
             url: "/thing/"+objectId,//hint to put or delete,
             data: _obj,
             dataType: "json" //JSON.parse invoked
           });
           req.done(function (obj) {
              //obj = {"updatedAt": "" }
              return good( obj );
           });
           req.fail(function(jqXHR, textStatus) {
              alert( "Request failed: " + textStatus );
           });
        }
      };
})();