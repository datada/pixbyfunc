(function () {
   if (window.GT) {} else {window.GT = {};}
   
   ////////////
   // CANADA
   ////////////
   
   var _CANADA_FED_TAX_LAW = {
      prepareFederalTaxReturn: function (input) {
        return {
           tax: "CAD0"
        }; 
      }
   };//Canada Federal
   
   var _CA_AB_TAX_LAW = {
      prepareProvincialTaxReturn: function ( fedReturn ) {
        return {
           tax: "CA-AB 0"
        }; 
      }
   };//Alberta
   
   var _CA_BC_TAX_LAW = {
      prepareProvincialTaxReturn: function ( fedReturn ) {
        return {
           tax: "CA-BC 0"
        }; 
      }
   };//British Columbia

   
   var _CA_ON_TAX_LAW = {
      prepareProvincialTaxReturn: function ( fedReturn ) {
        return {
           tax: "CA-ON 0"
        }; 
      }
   };//Ontario
   
   var _CA_QC_TAX_LAW = {
      prepareProvincialTaxReturn: function ( fedReturn ) {
        return {
           tax: "CA-QC 0"
        }; 
      }
   };//Quebec
   
   
   //////////
   // USA
   //////////
   
   var _US_FED_TAX_LAW = {
      prepareFederalTaxReturn: function (input) {
        return {
           tax: "USD0"
        }; 
      }
   };//USA
   
   var _US_CA_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-CA 0"
        }; 
      }
   };//CA
   
   var _US_CT_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-CT 0"
        }; 
      }
   };//CT
   
   var _US_FL_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-FL 0"
        }; 
      }
   };//FL
   
   var _US_IL_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-IL 0"
        }; 
      }
   };//IL
   
   var _US_MA_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-MA 0"
        }; 
      }
   };//MA   

   var _US_NY_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-NY 0"
        }; 
      }
   };//NY

   var _US_TX_TAX_LAW = {
      prepareStateTaxReturn: function ( fedReturn ) {
        return {
           tax: "US-TX 0"
        }; 
      }
   };//TX

   GT.getTaxJusrisdiction = function ( country ) {
     return {
        CA:{
           getFederalTaxLaw: function () {
              return _CANADA_FED_TAX_LAW;
           },
           getProvincialTaxLaw: function ( province ) {
              return {
                 "AB": _CA_AB_TAX_LAW,
                 "BC": _CA_BC_TAX_LAW,
                 "ON": _CA_ON_TAX_LAW,
                 "QC": _CA_QC_TAX_LAW
              }[ province ];
           }
        },
        US:{
           getFederalTaxLaw: function () {
              return _US_FED_TAX_LAW;
           },
           getStateTaxLaw: function ( state ) {
              return {
                 "CA": _US_CA_TAX_LAW,
                 "CT": _US_CT_TAX_LAW,
                 "FL": _US_FL_TAX_LAW,
                 "IL": _US_IL_TAX_LAW,
                 "MA": _US_MA_TAX_LAW,
                 "NY": _US_NY_TAX_LAW,
                 "TX": _US_TX_TAX_LAW
              }[ state ];
           }
        }
     }[ country ];
   };
   
})();