window.storage = (function () {
  
   return {
      loadThings: function (ownerId, good, bad) {
         
        try {
            var thing, things = [], 
                i, max = localStorage.length;

            for (i = 0; i < max; i+=1) {
                thing = localStorage.getItem(localStorage.key( i ));
                if (thing.ownerId === ownerId) things.push( thing );
            }

            return good( things );
        } catch (e) {
           if (bad) 
           return bad(e);
                      
           alert( "Request failed: " + e );
        }
      },

      createThing: function (_thing, good, bad) {

        try {
            var thing = _.clone(_thing); 
            var objectId = 'oid-'+Math.random();
            thing['objectId'] = objectId;
            thing['createdAt'] = new Date().toString();
            localStorage.setItem( objectId, JSON.stringify(thing) );//JSON.stringify if nec

            return good( thing );
        } catch (e) {
           if (bad) return bad(e);
                      
           alert( "Create Request failed: " + e );
        }
     
      },

      readThing: function (objectId, good, bad) {

        try {
            var _thing = localStorage.getItem( objectId );//JSON.stringify if nec
            return good( JSON.parse( _thing ) );
        } catch (e) {
           if (bad) return bad(e);  
           
           alert( "Read "+objectId+" Request failed: " + e );
        }
      },

      updateThing: function (thing, good, bad) {

        try {
           var objectId = thing.objectId;
           
           //keywords and some field not to be updated
           _.each(['createdAt', 'updatedAt', 'objectId', 'ownerId'], function (word) {
              delete thing[ word ];
           });
          
          var _thing = localStorage.getItem( objectId );
          _thing = JSON.parse(_thing);
          _.extend(_thing, thing);//incremental update....
          _thing.updatedAt = new Date().toString();
                   
          localStorage.setItem(objectId, _thing);

          return good( { updatedAt:_thing.updatedAt } );

        } catch (e) {
           if (bad) return bad(e);  
           
           alert( "Update Request failed: " + e );
        }

      },
      
      deleteThing: function (thing, good, bad) {
           
        try {
          var objectId = thing.objectId;           
          localStorage.removeItem( objectId );

          return good( {} );

        } catch (e) {
           if (bad) return bad(e);  
           
           alert( "Delete Request failed: " + e );
        }
      }
    };

})();