var identity = function (n) {
    var result = m_by_n(n,n);
    var i;
    for (i=0; i<n; i+=1) {
        result[i][i] = 1;
    }
    return result;
};

var transpose = function(matrix) {
  var transposed = [];
  
  var R = rowSize(matrix);
  var C = colSize(matrix);

  var r,c;
  for (c=0; c<C; c+=1) {//for each row of transposed matrixt
    transposed[c] = [];
    for (r=0; r<R; r+=1) {
      transposed[c][r] = matrix[r][c];
    }
  }
  return transposed;
};

var t = transpose;


