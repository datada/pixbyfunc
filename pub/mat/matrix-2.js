var rowSize = function (matrix) {
  return matrix.length;
};

var colSize = function (matrix) {
  return matrix[0].length;
};

var make_m_by_n = function (m, n, initial) {
  var tmp, result = [];
  var row, col;
  for (row=0; row<m; row+=1) {
     tmp = [];    
     for (col=0; col<n; col+=1) {
       tmp[col] = initial;
     }
     result.push(tmp);
  }
  return result;
};

var m_by_n = function (m, n, init) {
  init = (init === undefined) ? 0 : init;
  return make_m_by_n(m,n,init);
};

