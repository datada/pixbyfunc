
//naive per
//http://softwareengineering.vazexqi.com/2009/11/23/lessons-from-parallelizing-matrix-multiplication
var mat_mult = function (m, n, p, a, b, result) {
  var i,j,k;
  for (i=0; i<m; i+=1) {
    for (j=0; j<n; j+=1) {
      for (k=0; k<p; k+=1) {
        result[i][j] += a[i][k] * b[k][j];
      }
    }
  } 
  return result;
};

//(m by p) * (p by n)
var mult = function (a, b) {
  var m = a.length //row size of a
  ,n = b[0].length //col size of b
  ,p = b.length; // col size of a or row size of b 
  return mat_mult(m, n, p, a, b, m_by_n(m,n));
};