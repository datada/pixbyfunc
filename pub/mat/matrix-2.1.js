var rowSize = function (matrix) {
  return matrix.length;
};

var colSize = function (matrix) {
  return matrix[0].length;
};

var make_row = function (size, initial) {
    var i, row = [];
    for (i = 0; i < size; i += 1) {  
       row.push(initial);
    }
    return row;
};

var make_m_by_n = function (m, n, initial) { 
  var r, rows = [];
  for (r = 0; r < m; r += 1) {
     rows.push(make_row(n, initial));
  }
  return rows;
};

var m_by_n = function (m, n, init) {
  init = (init === undefined) ? 0 : init;
  return make_m_by_n(m,n,init);
};
