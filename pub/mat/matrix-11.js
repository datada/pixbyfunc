var one_at_pivot  = function (M, pivot) {
  //left matrix that will multiply the pivot row by 1/M[pivot]
  var oner = function (M, pivot) {
    var pivot_val = M[pivot.row][pivot.col];
  
    var result = identity(rowSize(M));
    result[pivot.row][pivot.row] = (1/pivot_val);
  
    return result;
  };
  
  return mult(oner(M,pivot), M);
};

var reducer = function (M, pivot) {
  var result = [];
  var R = rowSize(M);
  var C = colSize(M);
  var r,c;
  var factor;
  for (r=0; r<R; r+=1) {
    result[r] = [];
    factor = (r === pivot.row) ? 0 : M[r][pivot.col];
    for (c=0; c<C; c+=1) {
      //result[r][c] = [ -1, "*", factor, "*", "(", M[pivot.row][c], ")" ].join(" ");
      result[r][c] = -1 * factor * ( M[pivot.row][c] );      
    }
  }
  
  return result;
};

var reduce = function (M, pivot) {
  
  var swap = function (M, a, b) {
      var tmp = M[a];
      M[a] = M[b];
      M[b] = tmp;
      return M;
  };

  if (rowSize(M) <= pivot.row) {
    return M;
  }
  if (colSize(M) <= pivot.col) {
    return M;
  }
  
  //non zero from here and down
  var i = pivot.row;
  while (M[i][pivot.col] === 0) {
      i += 1;
      if (rowSize(M) === i) {
          //all zeros below
          pivot.row += 1;
          pivot.col += 1;
          return reduce(M,pivot);
      }
  }
  if (i !== pivot.row) {
    M = swap(M, pivot.row, i);
  }
  
  M = one_at_pivot(M, pivot);
  
  //make zeros at pivot columns
  M = add(M, reducer(M, pivot));

  pivot.row += 1;
  pivot.col += 1;
  
  return reduce(M, pivot);
};

var copy = function (M) {
  var rowS = rowSize(M);
  var colS = colSize(M);
  var result = m_by_n(rowS, colS, null);
  var r,c;
  for (r=0; r<rowS; r+=1) {
    for (c=0; c<colS; c+1) {
      result[r][c] = M[r][c];
    }
  }
  
  return result;
};

var inverse = function (M) {
 //append identy to the right
  var blend = function (A,B) {
    var result = {
        A:A,
        B:B,
        blended: [],
        unblend: function () {
          var rowS = rowSize(this.A);
          var start = colSize(this.A);
          var end = start + colSize(this.B);
          var result = [];
          var r,c;
          for (r=0; r<rowS; r+=1) {
            result[r] = [];
            for (c=start; c<end; c+=1) {
              result[r].push(this.blended[r][c]);
            }
          }
          return result;
        },
        end: ""
     };
   
    var rowS = rowSize(A);
    var r;
    for (r=0; r<rowS; r+=1) {
      result.blended[r] = A[r].concat(B[r]);
    }

    return result;
  };

  var result = blend(M, identity(rowSize(M)));//Jordanian....
  result.blended = reduce(result.blended, {row:0, col:0});

  return result.unblend();
};