
var transpose = function(a) {
  var row = a.length;
  var col = a[0].length;
  var result = make_m_by_n(col, row, 0);
  var r,c;
  for (r=0; r<row; r+=1) {
    for (c=0; c<col; c+=1) {
      result[c][r] = a[r][c];
    }
  }
  return result;
};