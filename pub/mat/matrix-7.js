

var eval_matrix = function (matrix) {
  var R = rowSize(matrix);
  var C = colSize(matrix);
  var r,c;
  
  for (r=0; r<R; r+=1) {
    for (c=0; c<C; c+=1) {
      matrix[r][c] = eval("("+matrix[r][c]+")");
    }
  }
  return matrix;
};

var force = eval_matrix;

// m x p p x n
var mult_delayed = function (M, P, N, left, right, result) {
  var m,p,m;
  for (m=0; m<M; m+=1) {
    for (p=0; p<P; p+=1) {
      for (n=0; n<N; n+=1) {
        result[m][n] += (result[m][n] ? " + " : "");
        result[m][n] +=  left[m][p]+" * "+right[p][n];
      }
    }
  } 

  return result;
};

var mult = function (a, b) {
  return mult_delayed(rowSize(a), colSize(a), colSize(b), a, b, make_m_by_n(rowSize(a),colSize(b),""));
};

var add_delayed = function (M, N, left, right, result) {
  var m,n;
  for (m=0; m<M; m+=1) {
    for (n=0; n<N; n+=1) {
        result[m][n] =  left[m][n]+" + "+right[m][n];
    }
  }

  return result;
};

var add = function (a,b) {
  return add_delayed(rowSize(a), colSize(a), a, b, make_m_by_n(rowSize(a),colSize(a),""));
};
