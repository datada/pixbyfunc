var mat_add_delayed = function (m, n, a, b, result) {
  var i,j;
  for (i=0; i<m; i+=1) {
    for (j=0; j<n; j+=1) {
        result[i][j] += (result[i][j] ? " + " : "");//"" so we do not want to display + sign
        result[i][j] +=  a[i][j]+" + "+b[i][j];
    }
  } 

  return result;
};

var add_delayed = function (a,b) {
  var m = a.length //row size of a
  ,n = a[0].length; // col size of a 
  return mat_add_delayed(m, n, a, b, make_m_by_n(m,n,""));
};