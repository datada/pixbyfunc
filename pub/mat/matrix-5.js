var eval_matrix = function (m) {
  var r, c;
  var row = m.length;
  var col = m[0].length;
  for (r=0; r<row; r+=1) {
    for (c=0; c<col; c+=1) {
      m[r][c] = eval(m[r][c]);
    }
  }
  return m;
};

var mat_mult_delayed = function (m, n, p, a, b, result) {
  var i,j,k;
  for (i=0; i<m; i+=1) {
    for (k=0; k<p; k+=1) {
      for (j=0; j<n; j+=1) {
        result[i][j] += (result[i][j] ? " + " : "");//"" so we do not want to display + sign
        result[i][j] +=  a[i][k]+" * "+b[k][j];
      }
    }
  } 

  return result;
};

var mult_delayed = function (a, b) {
  var m = a.length //row size of a
  ,n = b[0].length //col size of b
  ,p = b.length; // col size of a or row size of b 
  return mat_mult_delayed(m, n, p, a, b, make_m_by_n(m,n,""));
};
