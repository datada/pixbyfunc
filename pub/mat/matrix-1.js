var each_row = function (m, proc) {
  var r, max = m.length;
  for (r=0; r<max; r+=1) {
      proc(r, m[r]);
  } 
};

var make_printer = function (writer) {
  return function (m) {
    each_row(m, function (r, row) {
        writer(row.join(" "));
    });
  };
};