import webapp2

class MainPage(webapp2.RequestHandler):
    def get(self):
        try:
            self.redirect("/pub/index.html")
        except Exception, e:
            self.response.headers['Content-Type'] = 'text/html'
            self.response.out.write('<a href="/pub/index.html" title="Home">Home</a>')        
        return None




application = webapp2.WSGIApplication([
	    ('/', MainPage),
], debug=True)